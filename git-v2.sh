#!/bin/bash

echo "##########################"
echo "   Start Git Process"
echo "##########################"
git pull
git add .
git commit -m "Updates"
echo "#################"
echo "    Branch       "
echo "#################"
git checkout master
git rebase main
git checkout main
git push --all
echo "#######################"
echo "    Git Push Done      "
echo "#######################"

# echo "##########################"
# echo "   Start Git Process"
# echo "##########################"
# git pull
# git add .
# git commit -m "Updates"
# echo "#################"
# echo "    Branch       "
# echo "#################"
# git checkout main
# git rebase master
# git checkout master
# git push --all
# #git push -u origin master
# #git push -u origin main
# echo "#######################"
# echo "    Git Push Done      "
# echo "#######################"
